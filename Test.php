<?require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');


Class Test
{
	public function __construct() {
		if (!Bitrix\Main\Loader::includeModule('iblock')) {
			ShowError('Не подключен модуль iblock');
		}
	}

	public function getItems($params = []) {
		$arResult = [];
		$cache_id = serialize($params);
		$obCache = new CPHPCache;

		if ($obCache->InitCache($params['CACHE_TIME'], $cache_id, '/'))
		{
			$vars = $obCache->GetVars();
			$arResult = $vars['arResult'];
		}
		elseif ($obCache->StartDataCache())
		{
			$rsFields = CIBlockElement::GetList($params['SORT'], $params['FILTER'], false, false, $params['SELECT']);
			while ($arFields = $rsFields->Fetch()) {
				$arResult[] = $arFields;
			}

			$obCache->EndDataCache(array(
				'arResult' => $arResult,
			));
		}
		return $arResult;
	}
}

$test = new Test();

$res = $test->getItems([
	'CACHE_TIME' => 10000,
	'FILTER' => [
		'IBLOCK_ID' => 31,
	],
	'SELECT' => [
		'NAME'
	],
]);

echo '<pre>' . print_r($res, true) . '</pre>';